package count

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"strconv"
	"sync"
)

// Base interface for Counter
type Counter interface {
	Init(r io.Reader) error
	Next() int
	Save(w io.Writer, val string) error
	Reset(w io.Writer) error
}

// `in-memory` Implementation of `Counter` interface
type InMemCounter struct {
	Count int
	mu    sync.RWMutex
}

// Initialize counter from the reader
func (c *InMemCounter) Init(r io.Reader) error {
	var data bytes.Buffer
	_, err := io.Copy(&data, r)
	if err != nil {
		log.Println(fmt.Sprintf("Unable to Init Error: %s", err))
		return err
	}
	c.Count, err = strconv.Atoi(data.String())
	if err != nil {
		log.Println(fmt.Sprintf("Unable to convert data to int %s", data.String()))
		return err
	}
	return nil
}

// Provides next request counter
// synchronization is required when multiple request goRoutines accessing
func (c *InMemCounter) Next() int {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Count += 1
	return c.Count
}

// Saves the current counter value to writer
func (c *InMemCounter) Save(w io.Writer, val string) error {
	_, err := w.Write([]byte(val))
	if err != nil {
		log.Println(fmt.Sprintf("Unable to write data(%s)", strconv.Itoa(c.Count)))
		return err
	}
	return nil
}

// On each n seconds this counter resets to `0`
// Reset operation and request goRoutines may concurrently access the same memory
// Hence the lock
func (c *InMemCounter) Reset(w io.Writer) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Count = 0
	return c.Save(w, "0")
}
