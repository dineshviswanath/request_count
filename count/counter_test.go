package count_test

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"requestCount/count"
	"strings"
	"testing"
)

func TestInMemory_Init(t *testing.T) {
	c := count.InMemCounter{}
	c.Init(strings.NewReader("100"))
	assert.NotEqual(t, 0, c.Count)
	assert.Equal(t, 100, c.Count)
}

func TestInMemory_InitNotInteger(t *testing.T) {
	c := count.InMemCounter{}
	err := c.Init(strings.NewReader("a"))
	assert.NotEqual(t, 100, c.Count)
	assert.Error(t, err)
}

func TestInMemory_InitReaderEmpty(t *testing.T) {
	c := count.InMemCounter{}
	err := c.Init(strings.NewReader(""))
	assert.NotEqual(t, 100, c.Count)
	assert.Error(t, err)
}

func TestInMemory_Save(t *testing.T) {
	c := count.InMemCounter{Count: 100}
	var b bytes.Buffer
	c.Save(&b, "100")
	assert.Equal(t, "100", b.String())
}

func TestInMemory_Next(t *testing.T) {
	c := count.InMemCounter{Count: 100}
	next := c.Next()
	assert.Equal(t, 101, next)
}

func TestInMemory_Next_concurrent(t *testing.T) {
	c := count.InMemCounter{Count: 100}
	out := make(chan int, 1)

	go func() {
		out <- c.Next()
	}()
	go func() {
		out <- c.Next()
	}()

	assert.Equal(t, 101, <-out)
	assert.Equal(t, 102, <-out)
}

func TestInMemory_Reset(t *testing.T) {
	c := count.InMemCounter{Count: 100}
	var b bytes.Buffer
	c.Reset(&b)
	assert.Equal(t, "0", b.String())
	assert.Equal(t, 0, c.Count)
}
