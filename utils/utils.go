package utils

import (
	"log"
	"os"
)

// Utility function to clear the content of file before writing new content
func ClearFile(f *os.File) {
	f.Truncate(0)
	f.Seek(0, 0)
}

// Utility function to get
func GetFile(f string) *os.File {
	var file *os.File
	_, err := os.Stat(f)
	if err != nil {
		file, err = createFile(file, f)
	} else {
		file = openFile(file, f)
	}
	return file
}

func openFile(file *os.File, f string) *os.File {
	file, err := os.OpenFile(f, os.O_RDWR, 0644)
	if err != nil {
		log.Fatal("Unable to open file")
	}
	return file
}

func createFile(file *os.File, f string) (*os.File, error) {
	file, err := os.Create(f)
	if err != nil {
		log.Fatal("Unable to create file")
	}
	err = initFile(file, "0")
	return file, err
}

func initFile(file *os.File, val string) error {
	_, err := file.Write([]byte(val))
	if err != nil {
		log.Fatal("Unable to init file")
	}
	return err
}
