package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"requestCount/common"
	"requestCount/count"
	"requestCount/utils"
	"strconv"
	"time"
)

const FILE = "./data"
const TIMEOUT = 60
const PORT = ":5050"
const URL = "/count"

// Request store handler
type RequestHandler struct {
	count *count.InMemCounter
}

// Handles each request
func (rh *RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	res := common.NewCountResponse(rh.count.Next())
	res.RenderJSON(w)
}

func main() {
	log.Println(fmt.Sprintf("Request Counter...http://localhost%s%s", PORT, URL))

	f := utils.GetFile(FILE)

	c := &count.InMemCounter{}
	c.Init(f)

	rh := &RequestHandler{count: c}

	onTimeout(TIMEOUT, f, rh.count)
	onExit(f, rh)

	http.Handle(URL, rh)
	if err := http.ListenAndServe(PORT, nil); err != nil {
		log.Fatal("Server listening err")
	}
}

// Resets the state of counter on every `n` seconds
func onTimeout(timeout int, f *os.File, c *count.InMemCounter) {
	go func() {
		for {
			select {
			case <-time.After(time.Duration(timeout) * time.Second):
				log.Println("timeout: onTimeout")
				utils.ClearFile(f)
				c.Reset(f)
			}
		}
	}()
}

// Persist the state of counter before exit
func onExit(f *os.File, rh *RequestHandler) {
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt)
	go func() {
		<-exit
		utils.ClearFile(f)
		rh.count.Save(f, strconv.Itoa(rh.count.Count))
		log.Println("Saved in the file on exit")
		os.Exit(1)
	}()
}
