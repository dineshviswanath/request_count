package common

import (
	"encoding/json"
	"io"
)

// Base JSON response
type BaseResponse struct {
	Msg string `json:"msg"`
}

// Service specific JSON response
type CountResponse struct {
	BaseResponse
	Count int `json:"count"`
}

func (res *CountResponse) RenderJSON(w io.Writer) {
	json.NewEncoder(w).Encode(res)
}

func NewCountResponse(count int) *CountResponse {
	return &CountResponse{
		BaseResponse: BaseResponse{Msg: "OK"},
		Count:        count,
	}
}
