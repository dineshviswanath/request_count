# Request Counter

## What is it?

Using only the standard library,
create a Go HTTP server that on each request responds with a counter of the total number of requests 
that it has received during the previous 60 seconds (moving window). 
The server should continue to the return the correct numbers after restarting it, 
by persisting data to a file.



## Approach Explained:
* Used `in-memory` counter approach, to increment the counter on each request.
* Each request is a goRoutine, hence we have issue of concurrency. this is solved by having locks from `sync` package
* When application is killed, it has to restart with old state of counter. For this I have used `signal`  package to monitor the `interrupt` and write the state of counter to file
* For reseting the counter within window, I have started a `goroutine` from `main` function and monitor the `channel` . This channel get the input from `time` package and `time.After` function. This would reset the state of counter. Favoured this approach, as it could demonstrate go concurrency model

## Demo
![](demo.gif)


## Go tools
- [x] go Fmt
- [x] go vet
- [x] go test -race
- [x] go test with coverage for counter package

```
=== RUN   TestInMemory_Init
--- PASS: TestInMemory_Init (0.00s)
=== RUN   TestInMemory_InitNotInteger
2018/07/24 12:29:59 Unable to convert data to int a
--- PASS: TestInMemory_InitNotInteger (0.00s)
=== RUN   TestInMemory_InitReaderEmpty
2018/07/24 12:29:59 Unable to convert data to int
--- PASS: TestInMemory_InitReaderEmpty (0.00s)
=== RUN   TestInMemory_Save
--- PASS: TestInMemory_Save (0.00s)
=== RUN   TestInMemory_Next
--- PASS: TestInMemory_Next (0.00s)
=== RUN   TestInMemory_Next_concurrent
--- PASS: TestInMemory_Next_concurrent (0.00s)
=== RUN   TestInMemory_Reset
--- PASS: TestInMemory_Reset (0.00s)
PASS
coverage: 82.6% of statements
```


## How to run
1. Build the go file
    ```
    go build -o reqcnt main.go
    ```
2. Run the application
    ```
    ./reqcnt
    ```


## Things can be improved
- [ ] Logging
- [ ] Unit testing for other packages
- [ ] Moving app config to config file
- [ ] Flag parser to get port from command line
- [ ] Multi stag docker build file